<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "blogs".
 *
 * @property int $id
 * @property string $image
 * @property string $title_ru
 * @property string $title_en
 * @property string $content_ru
 * @property string $content_en
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Blogs extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blogs';
    }

    public function behaviors()
    {
        $parent = parent::behaviors();
        $parent[] = [
           'class' => TimestampBehavior::className()
        ];
        return $parent;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content_ru', 'content_en'], 'string'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['image', 'title_ru', 'title_en'], 'string', 'max' => 255],
            [['base_file'], 'safe']

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'base_file' => 'Image',
            'title_ru' => 'Title Ru',
            'title_en' => 'Title En',
            'content_ru' => 'Content Ru',
            'content_en' => 'Content En',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
