<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "web_settings".
 *
 * @property int $id
 * @property string $phone
 * @property string $address_ru
 * @property string $address_en
 * @property string $email
 * @property string $facebook_link
 * @property string $instagram_link
 * @property string $tg_link
 */
class WebSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'web_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone', 'address_ru', 'address_en', 'email', 'facebook_link', 'instagram_link', 'tg_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Phone',
            'address_ru' => 'Address Ru',
            'address_en' => 'Address En',
            'email' => 'Email',
            'facebook_link' => 'Facebook Link',
            'instagram_link' => 'Instagram Link',
            'tg_link' => 'Tg Link',
        ];
    }
}
