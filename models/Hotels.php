<?php

namespace app\models;

use PHPUnit\Util\Log\JSON;
use Yii;

/**
 * This is the model class for table "hotels".
 *
 * @property int $id
 * @property string $image
 * @property string $name_ru
 * @property string $name_en
 * @property string $content_ru
 * @property string $content_en
 * @property string $chars
 * @property string $characteristic
 * @property int $status
 *
 * @property Rooms[] $rooms
 */
class Hotels extends BaseModel
{

    public $chars;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hotels';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $parent = parent::behaviors();
        return $parent;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->characteristic = \yii\helpers\Json::encode($this->chars);
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->chars = \yii\helpers\Json::decode($this->characteristic);
        parent::afterFind();
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content_ru', 'content_en'], 'string'],
            [['status'], 'integer'],
            [['image', 'name_ru', 'name_en', 'characteristic'], 'string', 'max' => 255],
            [['base_file','chars'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'base_file' => 'Image',
            'name_ru' => 'Name Ru',
            'name_en' => 'Name En',
            'content_ru' => 'Content Ru',
            'content_en' => 'Content En',
            'characteristic' => 'Characteristic',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(Rooms::className(), ['hotel_id' => 'id']);
    }
}
