<?php

namespace app\models\api;
use app\models\ServiceImages;


/**
 * This is the model class for table "hotels".
 *
 */
class OneService extends \app\models\Service
{

    public function fields()
    {
        $fields = parent::fields();
        $fields['images'] = function ($model) {
            return $this->getAllimages($model->id);
        };
        unset($fields['status']);
        return $fields;
    }

    private function getAllimages($id)
    {
        $rooms = ServiceImages::find()->where(['service_id' => $id])->all();
        return $rooms;
    }
}
