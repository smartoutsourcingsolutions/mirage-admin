<?php

namespace app\models\api;


/**
 * This is the model class for table "hotels".
 *
 */
class Hotel extends \app\models\Hotels
{

    public function fields()
    {
        $fields = parent::fields();
        $fields['rooms'] = function ($model) {
            return $this->getAllRooms($model->id);
        };
        unset($fields['status']);
        return $fields;
    }

    private function getAllRooms($id)
    {
        $rooms = Room::find()->where(['hotel_id' => $id])->all();
        return $rooms;
    }
}
