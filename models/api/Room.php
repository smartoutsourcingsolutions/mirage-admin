<?php


namespace app\models\api;

use app\models\Characteristics;
use app\models\RoomImages;
use app\models\Rooms;
use yii\helpers\Json;

class Room extends Rooms
{
    public function fields()
    {
        $fields = parent::fields();
        $fields['characts'] = function ($model) {
            return $this->getCharacts($model->characteristics);
        };
        unset($fields['characteristics']);
        $fields['created_at'] = function ($model) {
            return date('d.m.Y', $model->created_at);
        };
        $fields['updated_at'] = function ($model) {
            return date('d.m.Y', $model->updated_at);
        };
        $fields['images'] = function ($model) {
            return $this->getImages($model->id);
        };


        return $fields;
    }

    private function getCharacts($characteristics)
    {
        $arr = [];
        if ($characteristics) {
            foreach (Json::decode($characteristics) as $item) {
                $charact = Characteristics::findOne((int)$item);
                $arr[] = $charact;
            }
        }

        return $arr;
    }

    private function getImages($id) {
        if($id)
        {
            $images = RoomImages::find()->where(['room_id' => $id])->all();
            if($images){
                return $images;
            }
        }
        return [];
    }

}