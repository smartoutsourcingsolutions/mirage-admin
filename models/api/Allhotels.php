<?php

namespace app\models\api;


use app\models\Characteristics;
use yii\helpers\Json;


/**
 * This is the model class for table "hotels".
 *
 */
class Allhotels extends \app\models\Hotels
{

    public function fields()
    {
        $fields = parent::fields();
        $fields['characts'] = function ($model) {
            return $this->getCharacts($model->characteristic);
        };
        unset($fields['characteristic']);
        return $fields;
    }

    private function getCharacts($characteristic)
    {
        $arr = [];
        foreach (Json::decode($characteristic) as $item) {
            $charact = Characteristics::findOne((int)$item);
            $arr[] = $charact;
        }
        return $arr;
    }
}
