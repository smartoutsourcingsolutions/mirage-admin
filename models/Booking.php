<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "booking".
 *
 * @property int $id
 * @property int $room_id
 * @property int $user_id
 * @property int $floor
 * @property int $from_date
 * @property int $end_date
 * @property int $days
 * @property int $adult
 * @property int $children
 * @property int $status
 *
 * @property Rooms $room
 * @property User $user
 */
class Booking extends \yii\db\ActiveRecord
{

    const BOOKED = 10;
    const RESERVED = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['room_id'], 'required'],
            [['room_id', 'user_id', 'floor', 'days', 'adult', 'children', 'status'], 'integer'],
            [['room_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rooms::className(), 'targetAttribute' => ['room_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['from_date', 'end_date',], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'room_id' => 'ID Комнаты',
            'user_id' => 'ID Пользователя',
            'floor' => 'Комната',
            'from_date' => 'Дата заезда',
            'end_date' => 'Дата выезда',
            'days' => 'Количество дней',
            'adult' => 'Взрослые',
            'children' => 'Дети',
            'status' => 'Статус брони',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Rooms::className(), ['id' => 'room_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    /** Rooms search
     * @param $from_date
     * @param $end_date
     * @param null $hotel_id
     * @param null $number
     * @return array
     */
    public function getRooms($from_date, $end_date, $hotel_id = null, $number = null)
    {
        $hotels = Hotels::find()->where(['status' => BaseModel::STATUS_ACTIVE])->all();
        if ($hotel_id)
            $hotels = Hotels::find()->where(['id' => $hotel_id])->asArray()->all();

        $res = [];
        foreach ($hotels as $hotel) {
            $rooms = Rooms::find()
                ->where(['hotel_id' => $hotel['id']])->all();

            if ($number)
                $rooms = Rooms::find()
                    ->where(['hotel_id' => $hotel['id']])
                    ->andWhere(['for_person' => $number])->all();

            foreach ($rooms as $room) {
                $booked = Booking::find()
                    ->where(['room_id' => $room->id])
                    ->andWhere(['>', 'from_date', time() - 24 * 60 * 60])
                    ->all();
                if ($booked) {
                    $check = $this->checkTime($booked, $from_date, $end_date);
                    if ($check)
                        $res[$hotel['id']][] = $room;
                } else {
                    $res[$hotel['id']][] = $room;
                }
            }
        }

        return $res;
    }

    public function checkTime($booked, $from_date, $end_date)
    {
        $res = true;
        foreach ($booked as $book) {
            if ($from_date > $book->end_date || $end_date < $book->from_date) {
                $res = true;
            } else {
                return false;
            }
        }
        return $res;
    }

    /** Save Booking from get request
     * @param $get
     * @param $userID
     * @return bool
     */
    public function saveOld($get, $userID)
    {
        $session = Yii::$app->session;
        $this->room_id = $session['roomID'];
        $this->user_id = $userID;
        $this->floor = $session['floor'];
        $this->from_date = strtotime($get['from_date']);
        $this->end_date = strtotime($get['end_date']);
        $this->days = (strtotime($get['end_date']) - strtotime($get['from_date'])) / 86400; // 60*60*24
        $this->adult = (int)$get['adult'];
        $this->children = (int)$get['children'];
        $this->status = self::BOOKED;
        if ($this->save()) {
            return $this->id;
        }
        return false;
    }

    public function booking($get, $userID = null)
    {
        $this->room_id = $get['room_id'];
        $this->user_id = !empty($userID) ? $userID : $get['user_id'];
        $this->floor = $get['floor'];
        $this->from_date = strtotime($get['from_date']);
        $this->end_date = strtotime($get['end_date']);
        $this->days = ($this->end_date - $this->from_date) / 60 * 60 * 24;
        $this->adult = $get['adult'];
        $this->children = $get['children'];
        $this->status = self::RESERVED;
        if ($this->save()) {
            return true;
        }
        return false;
    }

}
