<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Json;

/**
 * This is the model class for table "rooms".
 *
 * @property int $id
 * @property int $hotel_id
 * @property string $image
 * @property string $title_ru
 * @property string $title_en
 * @property string $info_ru
 * @property string $info_en
 * @property string $content_ru
 * @property string $content_en
 * @property int $price
 * @property int $for_person
 * @property int $breakfast
 * @property string $characteristics
 * @property int $created_at
 * @property int $updated_at
 * @property int $chars
 *
 * @property RoomImages[] $roomImages
 * @property Hotels $hotel
 */
class Rooms extends BaseModel
{

    public $chars;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rooms';
    }

    public function behaviors()
    {
        $parent = parent::behaviors();
        $parent[] =
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_files',
                'multiple' => true,
                'uploadRelation' => 'roomImages',
                'pathAttribute' => 'image',
                'baseUrlAttribute' => false,
            ];
        $parent[] = [
            'class' => TimestampBehavior::className()
        ];

        return $parent;
    }

    public function beforeSave($insert)
    {
        $this->characteristics = \yii\helpers\Json::encode($this->chars);
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->chars = \yii\helpers\Json::decode($this->characteristics);
        parent::afterFind();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hotel_id', 'price', 'for_person'], 'required'],
            [['hotel_id', 'price', 'for_person', 'breakfast', 'created_at', 'updated_at', 'square'], 'integer'],
            [['info_ru', 'info_en', 'content_ru', 'content_en', 'characteristics'], 'string'],
            [['image', 'title_ru', 'title_en'], 'string', 'max' => 255],
            [['hotel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hotels::className(), 'targetAttribute' => ['hotel_id' => 'id']],
            [['base_file', 'base_files', 'chars'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hotel_id' => 'Hotel ID',
            'image' => 'Image',
            'square' => 'Square',
            'title_ru' => 'Title Ru',
            'title_en' => 'Title En',
            'info_ru' => 'Info Ru',
            'info_en' => 'Info En',
            'content_ru' => 'Content Ru',
            'content_en' => 'Content En',
            'price' => 'Price',
            'for_person' => 'For Person',
            'breakfast' => 'Breakfast',
            'characteristics' => 'Characteristics',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomImages()
    {
        return $this->hasMany(RoomImages::className(), ['room_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotels::className(), ['id' => 'hotel_id']);
    }

}
