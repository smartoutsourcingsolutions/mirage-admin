<?php

namespace app\models\search;

use app\models\Booking;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Books;

/**
 * BooksSearch represents the model behind the search form of `app\models\Books`.
 */
class BookingSearch extends Booking
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['room_id', 'user_id', 'floor', 'from_date', 'end_date', 'days', 'adult', 'children', 'status'], 'safe'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$hotelId = null)
    {
        $query = Booking::find()
            ->orderBy(['id' => SORT_DESC]);
        if($hotelId)
            $query->joinWith('room')
                ->where(['rooms.hotel_id' => $hotelId]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//             $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'room_id' => $this->room_id,
            'user_id' => $this->user_id,
            'floor' => $this->floor,
            'days' => $this->days,
            'adult' => $this->adult,
            'children' => $this->children,
            'status' => $this->status,
        ]);

        if (!empty($this->from_date)) {
            $this->from_date = date('d-m-Y', strtotime($this->from_date));
            $query->andFilterWhere(['>', 'FROM_UNIXTIME(from_date, "%d-%m-%Y")', $this->from_date]);
        }
        if (!empty($this->end_date)) {
            $this->end_date = date('d-m-Y', strtotime($this->end_date));
            $query->andFilterWhere(['<', 'FROM_UNIXTIME(end_date, "%d-%m-%Y")', $this->end_date]);
        }

//        debug($this->from_date,1);
//        die();
//        if($this->from_date)
//            $query->andFilterWhere(['<', 'from_date', strtotime($this->from_date)]);
//        if($this->end_date)
//            $query->andFilterWhere(['<', 'end_date', strtotime($this->end_date)]);


        return $dataProvider;
    }
}
