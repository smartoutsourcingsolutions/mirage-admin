<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property string $image
 * @property string $title_ru
 * @property string $title_en
 * @property string $content_ru
 * @property string $content_en
 * @property int $status
 *
 * @property ServiceImages[] $serviceImages
 */
class Service extends BaseModel
{

    public $images;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service';
    }


    public function afterFind()
    {
        $this->images = $this->getServiceImages();
        parent::afterFind();
    }

    public function behaviors()
    {
        $parent = parent::behaviors();
        $parent[] =
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_files',
                'multiple' => true,
                'uploadRelation' => 'serviceImages',
                'pathAttribute' => 'img',
                'baseUrlAttribute' => false,
            ];

        return $parent;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_en'], 'required'],
            [['content_ru', 'content_en'], 'string'],
            [['status'], 'integer'],
            [['image', 'title_ru', 'title_en'], 'string', 'max' => 255],
            [['base_file','base_files','images'],'safe']
        ];

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'base_file' => 'Main Image',
            'base_files' => 'Images',
            'title_ru' => 'Title Ru',
            'title_en' => 'Title En',
            'content_ru' => 'Content Ru',
            'content_en' => 'Content En',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceImages()
    {
        return $this->hasMany(ServiceImages::className(), ['service_id' => 'id']);
    }
}
