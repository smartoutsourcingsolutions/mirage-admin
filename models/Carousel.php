<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "carousel".
 *
 * @property int $id
 * @property string $image
 * @property string $title_ru
 * @property string $title_en
 * @property string $description_ru
 * @property string $description_en
 * @property int $order
 * @property int $status
 */
class Carousel extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'carousel';
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\app\components\FileUploadBehaviour'
            ],

        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_en'], 'required'],
            [['order', 'status'], 'integer'],
            [['title_ru', 'title_en', 'description_ru', 'description_en'], 'string', 'max' => 255],
            [['image'], 'file']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'title_ru' => 'Title Ru',
            'title_en' => 'Title En',
            'description_ru' => 'Description Ru',
            'description_en' => 'Description En',
            'order' => 'Order',
            'status' => 'Status',
        ];
    }
}
