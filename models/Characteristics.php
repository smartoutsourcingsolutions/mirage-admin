<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "characteristics".
 *
 * @property int $id
 * @property string $svg
 * @property string $name_ru
 * @property string $name_en
 * @property int $status
 */
class Characteristics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'characteristics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['svg'], 'string'],
            [['status'], 'integer'],
            [['name_ru', 'name_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'svg' => 'Svg',
            'name_ru' => 'Name Ru',
            'name_en' => 'Name En',
            'status' => 'Status',
        ];
    }


    public function getGroups()
    {
        $characts = $this::find()->where(['status' => BaseModel::STATUS_ACTIVE])->all();
        $arr = [];
        foreach ($characts as $item){
            $arr[$item->id] = $item->name_ru;
        }
        return $arr;
    }
}
