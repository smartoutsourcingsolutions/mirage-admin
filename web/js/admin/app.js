$('.dropify').dropify({
    messages: {
        'default': 'Drag and drop a file here or click',
        'replace': 'Drag and drop or click to replace',
        'remove': 'Remove',
        'error': 'Ooops, something wrong happended.'
    }
});

var tag = $('#admin-content p:first:has("a")');
$('.page-titles .col-md-5.align-self-center').html(tag.html());
tag.remove();

$(document).on('submit', '#search', function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
        url: form.attr('action'),  // Url::to(['/booking/search'])
        type: form.attr('method'), // GET
        data: form.serialize(),    //

        success: function (res) {
            $('#hotels').html(res);
        },
        error: function (res) {
            alert('Ошибка сервера');
        }
    })

});

$(document).on('click', '.roomView', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
        url: '/admin/booking/rooms',  // Url::to(['/booking/search'])
        type: 'get', // GET
        data: {id: id},    //

        success: function (res) {
            $('#viewContent').html(res);
            $("#viewModal").modal();
        },
        error: function (res) {
            alert('Ошибка сервера');
        }
    })
});

$(document).on('click', '.order', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
        url: '/admin/booking/booking',  // Url::to(['/booking/search'])
        type: 'get', // GET
        data: {id: id},    //

        success: function (res) {
            $('#viewContent').html(res);
            $("#viewModal").modal();
        },
        error: function (res) {
            alert('Ошибка сервера');
        }
    })
});

$(document).on('blur', '.fioSearch', function () {
    setTimeout(function () {
        $('.search_list ul').removeClass('active')
    },100)
});

$(document).on('input', '.fioSearch', function (e) {
    e.preventDefault();
    var search = $(this).val();
    if(search.length > 0){
        $.ajax({
            url: '/admin/booking/fio-search',  // Url::to(['/booking/search'])
            type: 'get', // GET
            data: {search: search},    //
            success: function (res) {
                $('.search_list ul').addClass('active').html(res);
            },
            error: function (res) {
                alert('Ошибка сервера');
            }
        })
    } else {
        $('.search_list ul').removeClass('active')
    }



});

$(document).on('click', '.search_list li', function () {
    var id = $(this).data('id');
    var text = $(this).text();
    $('.fioname').attr('value', id);
    $('.fioSearch').val(text);
    $('.search_list ul').removeClass('active')
});

$(document).on('click','.zabron',function (e) {
   e.preventDefault();
   var id = $(this).data('id');
   var spans = $(this).closest('span');
    $.ajax({
        url: '/admin/booking/change-status',
        type: 'get', // GET
        data: {id: id},    //
        success: function (res) {
            if(res == true)
               spans.html('<label class="label label-info bron-label">Забронирован</label>')
        },
        error: function (res) {
            alert('Ошибка сервера');
        }
    })
});