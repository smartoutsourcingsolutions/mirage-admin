<?php

/* @var $this \yii\web\View */
/* @var $content string */

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang=en>
<head>
    <meta charset=UTF-8>
    <title>The Mirage Hotel</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link href=/static/css/app.24602461f5d1d3f140b6b32e9a0762b6.css rel=stylesheet>
</head>
<body>
<?php $this->beginBody() ?>
<?= $content ?>
<script type=text/javascript src=/static/js/manifest.2ae2e69a05c33dfc65f8.js></script>
<script type=text/javascript src=/static/js/vendor.845f2a59ba3be27589f9.js></script>
<script type=text/javascript src=/static/js/app.69d993a67b931cedd670.js></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
