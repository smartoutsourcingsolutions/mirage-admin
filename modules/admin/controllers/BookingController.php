<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 01.04.2019
 * Time: 17:01
 */

namespace app\modules\admin\controllers;


use app\models\api\Hotel;
use app\models\BaseModel;
use app\models\Booking;
use app\models\Characteristics;
use app\models\forms\Signup;
use app\models\Hotels;
use app\models\Rooms;
use app\models\search\BookingSearch;
use app\models\User;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class BookingController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $books = Booking::find()->orderBy(['id' => SORT_DESC])->limit(15)->all();
        $hotels = Hotels::find()->where(['status' => BaseModel::STATUS_ACTIVE])->all();
        $booking = new Booking();

        if (Yii::$app->request->get()) {
            $bookId = false;
            // If Request GET
            if (Yii::$app->request->get('searchfio')) {
                $bookId = $booking->saveOld(Yii::$app->request->get(), Yii::$app->request->get('fullname'));
            } else {
                $user = new Signup();
                $userId = $user->saveNew(Yii::$app->request->get());
                if ($userId) {
                    $bookId = $booking->saveOld(Yii::$app->request->get(), $userId);
                }
            }
            // If success
            if ($bookId) {
                Yii::$app->session->setFlash('booked');
                return $this->redirect(Url::to(['/admin/booking/view', 'id' => $bookId]));
            }
        }

        return $this->render('index', [
            'books' => $books,
            'hotels' => $hotels
        ]);
    }

    /**
     * @return string
     */
    public function actionSearch()
    {
        $book = new Booking();
        $form = Yii::$app->request->get();
        Yii::$app->session->open();
        $session = Yii::$app->session;
        $session->set('from_date', $form['from_date']);
        $session->set('end_date', $form['end_date']);
        $hotels = $book->getRooms(strtotime($form['from_date']), strtotime($form['end_date']), $form['hotel_id'], $form['number']);
        return $this->renderAjax('hotels', ['hotels' => $hotels]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionRooms($id)
    {
        $room = Rooms::findOne($id);
        $charact = Characteristics::find()->where(['id' => Json::decode($room->characteristics)])->all();
        return $this->renderAjax('roomAjax', [
            'room' => $room,
            'charact' => $charact
        ]);
    }

    public function actionBooking($id)
    {
        $session = Yii::$app->session;
        $room = Rooms::findOne($id);
        $session->set('roomID', $id);
        $session->set('floor', $room->for_person);
        $user = User::find()->orderBy(['id' => SORT_DESC])->limit(1)->one();
        $username = 'guest' . ($user->id + 1);

        return $this->renderAjax('booking', [
            'from' => $session['from_date']?: '',
            'end' =>  $session['end_date'] ?: '',
            'room' => $room,
            'username' => $username
        ]);
    }

    public function actionFioSearch($search)
    {
        $users = User::find()
            ->where(['like', 'fullname', $search])
            ->limit(10)
            ->all();

        return $this->renderAjax('fioAjax', ['users' => $users]);

    }


    /** All Bookings
     * @return string
     */
    public function actionBookedRooms()
    {
        $searchModel = new BookingSearch();
        $hotelId = Yii::$app->request->get('hotel');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$hotelId);
        $hotels = Hotels::find()->all();

        return $this->render('booked-rooms', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'hotels' => $hotels
        ]);
    }

    /** View One Bookings
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $book = Booking::findOne($id);
        $charact = '';
        if (!empty($book->room)) {
            $charact = Characteristics::find()->where(['id' => Json::decode($book->room->characteristics)])->all();
        }
        return $this->render('booked', [
            'book' => $book,
            'charact' => $charact
        ]);
    }
    /**
     * Deletes an existing Books model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['booked-rooms']);
    }
    /**
     * Updates an existing Books model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
           $model->from_date = strtotime($model->from_date);
           $model->end_date = strtotime($model->end_date);
           $model->days = ($model->end_date- $model->from_date) / 86400; // 60*60*24
            if($model->save())
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    /**
     * Finds the Books model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Booking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Booking::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionChangeStatus($id){
        $book = Booking::findOne($id);
        if($book){
            $book->status = Booking::BOOKED;
            if($book->save(false)){
                return true;
            }
        }
        return false;
    }


}