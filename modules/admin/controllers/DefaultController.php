<?php

namespace app\modules\admin\controllers;

use app\models\Booking;
use app\models\Books;
use app\models\forms\Signup;
use app\models\User;
use mdm\admin\models\form\Login;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        $book = new Booking();
        $today = date('d-m-Y',time());
        $tomorrow = date('d-m-Y',(time() + 60*60*24));
        $hotels = $book->getRooms(strtotime($today), strtotime($tomorrow));
        $count = 0;
        foreach ($hotels as $item){
            $count += count($item);
        }

        $user= User::find()->all();
        $books = Booking::find()->orderBy(['id' => SORT_DESC])->limit(15)->all();
        return $this->render('index',[
            'books' => $books,
            'user' => $user,
            'count' => $count
        ]);
    }

    /**
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'login';
        $model = new Login();

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateUser()
    {
        $model = new Signup();
        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($user = $model->signupManager()) {
                return $this->redirect('index');
            }
        }

        return $this->render('create-user', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->getUser()->logout();

        return $this->goHome();
    }
}
