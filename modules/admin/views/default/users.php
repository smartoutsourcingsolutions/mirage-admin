<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 10.04.2019
 * Time: 15:00
 */

?>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>


                        <?php if (!empty($user)) {
                            foreach ($users as $k => $item):?>
                                <tr>
                                    <td><?= ++$k ?></td>
                                    <td><?= $item->username ?></td>
                                    <td><?= $item->email ?></td>
                                    <td><?= $item->status ?></td>
                                    <td><?= $item->status ?></td>

                                </tr>
                            <?php endforeach;
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

