<?php
use app\models\Booking;
use yii\helpers\Url;

?>
<!-- Start Page Content -->
<div class="row">
    <div class="col-md-4">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-hotel f-s-40 color-primary"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?=  $count ?></h2>
                    <p class="m-b-0">Свободные комнаты на сегодня</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-shopping-cart f-s-40 color-success"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= count($books) ?></h2>
                    <p class="m-b-0">Общее количетсво бронов</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-user f-s-40 color-warning"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= count($user) ?></h2>
                    <p class="m-b-0">Колчетсов Зарегистрированных пользователей</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-title">
                <h4><?= Yii::t('app','Последные брони') ?></h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Гостиница</th>
                            <th>Комната</th>
                            <th>Бронировал</th>
                            <th>Телефон</th>
                            <th>Дата заезда</th>
                            <th>Дата выезда</th>
                            <th>День</th>
                            <th>Номер</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>


                        <?php if (!empty($books)) {
                            foreach ($books as $k => $item):?>
                                <tr>
                                    <td><?= ++$k ?></td>
                                    <td><?= !empty($item->room->hotel->name_ru) ? $item->room->hotel->name_ru : '' ?></td>
                                    <td><?= !empty($item->room->title_ru) ? $item->room->title_ru : '' ?></td>
                                    <td><?= !empty($item->user->fullname) ? $item->user->fullname : '' ?></td>
                                    <td><?= !empty($item->user->phone) ? $item->user->phone : '' ?></td>
                                    <td><?= date('d-m.Y',$item->from_date) ?></td>
                                    <td><?= date('d-m.Y',$item->end_date) ?></td>
                                    <td><?= $item->days ?></td>
                                    <td><?= !empty($item->room->for_person) ? $item->room->for_person : '' ?></td>
                                    <td><?= $item->status == Booking::BOOKED
                                            ?  '<label class="label label-info">'.Yii::t('app','Забронирован') .'</label>'
                                            :  '<label class="label label-warning">'.Yii::t('app','Зарезервированный'). '</label>' ?></td>
                                    <td><a href="<?= Url::to(['/admin/booking/view', 'id' => $item->id]) ?>"
                                           class="btn btn-info"><?= Yii::t('app','Посмотреть брон') ?></a></td>
                                </tr>
                            <?php endforeach;
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- End PAge Content -->