<?php
/**
 * Created by PhpStorm.
 * User: Farhodjon
 * Date: 10.03.2018
 * Time: 15:17
 */

use app\modules\admin\widgets\Menu;
use yii\helpers\Url;

?>
<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <?php

            if(Yii::$app->user->can('admin')):
                try {
                    echo Menu::widget([
                        'options' => ['id' => 'sidebarnav'],
                        'submenuTemplate' => "\n<ul aria-expanded='false' class='collapse'>\n{items}\n</ul>\n",
                        'badgeClass' => 'label label-rouded label-primary pull-right',
                        'activateParents' => true,
                        'items' => [
                            [
                                'label' => '',
                                'options' => ['class' => 'nav-devider']
                            ],
                            [
                                'label' => 'Home',
                                'options' => ['class' => 'nav-label']
                            ],
                            [
                                'label' => 'Dashboard',
                                'url' => ['default/index'],
                                'icon' => '<i class="fa fa-tachometer"></i>',
                            ],
                            [
                                'label' => 'Booking',
                                'url' => ['booking/index'],
                                'icon' => '<i class="fa fa-bookmark"></i>',
                            ],
                            [
                                'label' => 'Booking List',
                                'url' => ['booking/booked-rooms'],
                                'icon' => '<i class="fa fa-list"></i>',
                            ],
                            [
                                'label' => 'App',
                                'options' => ['class' => 'nav-label']
                            ],
                            [
                                'label' => 'Info',
                                'url' => '#',
                                'icon' => '<i class="fa fa-file-text-o"></i>',
                                'items' => [
                                    [
                                        'label' => 'Carousel',
                                        'url' => ['carousel/index'],
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],
                                    [
                                        'label' => 'Site Info',
                                        'url' => ['web-settings/view', 'id' => 1],
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],
                                    [
                                        'label' => 'Blogs',
                                        'url' => ['blogs/index'],
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],
                                    [
                                        'label' => 'Services',
                                        'url' => ['service/index'],
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],

                                ]
                            ],
                            [
                                'label' => 'Hotels',
                                'url' => '#',
                                'icon' => '<i class="fa fa-h-square"></i>',
                                'items' => [
                                    [
                                        'label' => 'Hotels',
                                        'url' => ['hotels/index'],
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],
                                    [
                                        'label' => 'Rooms',
                                        'url' => ['rooms/index'],
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],
                                    [
                                        'label' => 'Characteristics',
                                        'url' => ['characteristics/index'],
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],

                                ]
                            ],
                            [
                                'label' => 'Users',
                                'url' => '#',
                                'icon' => '<i class="fa fa-user"></i>',
                                'items' => [
                                    [
                                        'label' => 'Create Manager',
                                        'url' => ['/admin/default/create-user'],
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],
                                    [
                                        'label' => 'User roles',
                                        'url' => ['/roles/user'],
                                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    ],
                                ]
                            ]

                        ]
                    ]);
                } catch (Exception $e) {
                }
            elseif (Yii::$app->user->can('menagers')):
                try {
                    echo Menu::widget([
                        'options' => ['id' => 'sidebarnav'],
                        'submenuTemplate' => "\n<ul aria-expanded='false' class='collapse'>\n{items}\n</ul>\n",
                        'badgeClass' => 'label label-rouded label-primary pull-right',
                        'activateParents' => true,
                        'items' => [
                            [
                                'label' => '',
                                'options' => ['class' => 'nav-devider']
                            ],
                            [
                                'label' => 'CRM',
                                'options' => ['class' => 'nav-label']
                            ],
                            [
                                'label' => 'Главная',
                                'url' => ['default/index'],
                                'icon' => '<i class="fa fa-tachometer"></i>',
                            ],
                            [
                                'label' => 'Забранировать',
                                'url' => ['booking/index'],
                                'icon' => '<i class="fa fa-bookmark"></i>',
                            ],
                            [
                                'label' => 'Список бронов',
                                'url' => ['booking/booked-rooms'],
                                'icon' => '<i class="fa fa-list"></i>',
                            ],
                        ]
                    ]);
                } catch (Exception $e) {
                }
            endif;


            ?>
        </nav>
    </div>
</div>
