<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 04.04.2019
 * Time: 12:22
 */

use app\models\Characteristics;

?>

<?php if (!empty($room)): ?>
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Информация о номере</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <table class="table">
                <tbody>
                <tr>
                    <td><b><?= Yii::t('app','Гостиница') ?>:</b></td>
                    <td><?= $room->hotel->name_ru ?></td>
                </tr>
                <tr>
                    <td><b><?= Yii::t('app','Навзвание') ?>:</b></td>
                    <td><?= $room->title_ru ?></td>
                </tr>
                <tr>
                    <td><b><?= Yii::t('app','Информация') ?>:</b></td>
                    <td><?= strip_tags($room->info_ru) ?></td>
                </tr>
                <tr>
                    <td><b><?= Yii::t('app', 'Цена') ?>:</b></td>
                    <td> <?= $room->price ?>$</td>
                </tr>
                <tr>
                    <td><b><?= Yii::t('app', 'Завтрак') ?>:</b></td>
                    <td><?= $room->breakfast == 1 ? Yii::t('app', 'Включен') : Yii::t('app', 'Не включен') ?></td>
                </tr>
                <tr>
                    <td><b>Площадь:</b></td>
                    <td> <?= $room->square ?> кв. м</td>
                </tr>
                <tr>
                    <td><b><?= Yii::t('app', 'Доп инфо') ?>:</b></td>
                    <td><?php if($charact):
                                foreach ($charact as $item){
                                 echo $item->name_ru . ',';
                                }
                        endif;  ?></td>
                </tr>
                </tbody>
            </table>
            <?php if (!empty($room->image)): ?>
                <div style="text-align: center">

                    <img src="/uploads/<?= $room->image ?>" width="60%">
                </div>
            <?php endif; ?>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            <button type="button" class="btn btn-primary order" data-id="<?= $room->id ?>">Забронировать номер</button>
        </div>
    </div>

<?php endif; ?>