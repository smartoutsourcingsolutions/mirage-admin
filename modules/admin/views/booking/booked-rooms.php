<?php

use app\models\Booking;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Брони');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-md-2"><?= Yii::t('app', 'Гостинцы: ') ?></div>
            <?php foreach ($hotels as $item): ?>
                <a class="btn btn-success"
                   href="<?= \yii\helpers\Url::current(['/admin/booking/booked-rooms', 'hotel' => $item->id]) ?>"><?= $item->name_ru ?></a>&nbsp;
            <?php endforeach ?>
    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'room_id',
                'value' => function ($model) {
                    return $model->room->title_ru;
                },
                'format' => 'html',
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Rooms::find()->all(), 'id', 'title_ru')
            ],
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return !empty($model->user->fullname) ? $model->user->fullname : $model->user_id;
                }
            ],
            'floor',
            [
                'attribute' => 'from_date',
                'value' => function ($model) {
                    return date('d.m.Y', $model->from_date);
                },
                'format' => 'date'
            ],
            [
                'attribute' => 'end_date',
                'value' => function ($model) {
                    return date('d.m.Y', $model->end_date);
                },
                'format' => 'date'
            ],
            'days',
            'adult',
            'children',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->status == Booking::BOOKED
                        ? '<label class="label label-info bron-label">' . Yii::t('app', 'Забронирован') . '</label>'
                        : '<span class="rezerv"><label class="label label-default bron-label">' . Yii::t('app', 'Зарезервирован') . '</label>
                            <button class="btn btn-info zabron" data-id="'. $model->id .'">Забранировать</button></span>';
                },
                'filter' => [
                    Booking::BOOKED => Yii::t('app', 'Забронирован'),
                    Booking::RESERVED => Yii::t('app', 'Зарезервированный')
                ],
                'format' => 'raw'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a(Html::tag('i', '', ['class' => 'mdi mdi-eye']) . '', $url, ['class' => 'btn btn-success']);
                    },
                    'update' => function ($url, $model) {
                        return Html::a(Html::tag('i', '', ['class' => 'mdi mdi-pencil']) . '', $url, ['class' => 'btn btn-primary']);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(Html::tag('i', '', ['class' => 'mdi mdi-delete']) . '', $url, [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ]
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
