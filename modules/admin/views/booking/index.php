<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 01.04.2019
 * Time: 17:03
 */

use app\models\Booking;
use yii\helpers\Url;
use yii\jui\DatePicker;

?>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-title" style="text-align: center; margin-bottom: 10px">
                <h4>ЗАБРАНИРОВАТЬ НОМЕР</h4>
            </div>
            <div class="card-body">
                <form id="search" action="<?= Url::to(['/admin/booking/search']) ?>" method="get">
                    <div class="row">
                        <div class="col form-group">
                            <label class="control-label"><?= Yii::t('app', 'Гостиница') ?></label>
                            <select name="hotel_id" class="form-control">
                                <?php foreach ($hotels as $item): ?>
                                    <option value="<?= $item->id ?>"><?= $item->name_ru ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col form-group">
                            <label class="control-label"><?= Yii::t('app', 'Дата заезда') ?></label>
                            <?php echo DatePicker::widget([
                                'name' => 'from_date',
                                'language' => 'ru',
                                'value' => date('d-m.Y',time()),
                                'options' => [
                                    'class' => 'form-control',
                                    'placeholder' => 'Выберите день заезда',
                                    'required' => 'required'
                                ],
                                'clientOptions' => [
                                    'minDate' => '+0',
                                ],
                                'dateFormat' => 'dd-MM-yyyy',
                            ]); ?>
                        </div>
                        <div class="col form-group">
                            <label class="control-label"><?= Yii::t('app', 'Дата выезда') ?></label>
                            <?php echo DatePicker::widget([
                                'name' => 'end_date',
                                'language' => 'ru',
                                'value' => date('d-m.Y',(time()+60*60*24)),
                                'options' => [
                                    'class' => 'form-control',
                                    'placeholder' => 'Выберите день выезда',
                                    'required' => 'required'
                                ],
                                'clientOptions' => [
                                    'minDate' => '+1',
                                ],
                                'dateFormat' => 'dd-MM-yyyy',
                            ]); ?>
                        </div>
                        <div class="col form-group">
                            <label class="control-label">Номер</label>
                            <select name="number" class="form-control">
                                <option value="1">1 <?= Yii::t('app', 'Номер') ?></option>
                                <option value="2">2 <?= Yii::t('app', 'Номер') ?></option>
                                <option value="3">3 <?= Yii::t('app', 'Номер') ?></option>
                            </select>
                        </div>
                    </div>
                    <button class="btn btn-success" style="float: right">Поиск</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12" id="hotels">
        <div class="card">
            <div class="card-title">
                <h4>Последные брони</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Гостиница</th>
                            <th>Комната</th>
                            <th>Бронировал</th>
                            <th>Телефон</th>
                            <th>Дата заезда</th>
                            <th>Дата выезда</th>
                            <th>День</th>
                            <th>Номер</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($books)) {
                            foreach ($books as $k => $item):?>
                                <tr>
                                    <td><?= ++$k ?></td>
                                    <td><?= !empty($item->room->hotel->name_ru) ? $item->room->hotel->name_ru : '' ?></td>
                                    <td><?= !empty($item->room->title_ru) ? $item->room->title_ru : '' ?></td>
                                    <td><?= !empty($item->user->fullname) ? $item->user->fullname : '' ?></td>
                                    <td><?= !empty($item->user->phone) ? $item->user->phone : '' ?></td>
                                    <td><?= date('d-m.Y',$item->from_date) ?></td>
                                    <td><?= date('d-m.Y',$item->end_date) ?></td>
                                    <td><?= $item->days ?></td>
                                    <td><?= !empty($item->room->for_person) ? $item->room->for_person : '' ?></td>
                                    <td><?= $item->status == Booking::BOOKED
                                            ?  '<label class="label label-info">'.Yii::t('app','Забронирован') .'</label>'
                                            :  '<label class="label label-warning">'.Yii::t('app','Зарезервированный'). '</label>' ?></td>
                                    <td><a href="<?= Url::to(['/admin/booking/view', 'id' => $item->id]) ?>"
                                           class="btn btn-info"><?= Yii::t('app','Посмотреть брон') ?></a></td>
                                </tr>
                            <?php endforeach;
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

