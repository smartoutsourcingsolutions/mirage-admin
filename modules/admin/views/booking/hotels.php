<?php

use yii\helpers\Url;

?>

<div class="card">
    <div class="card-title">
        <h4>Результаты поиска</h4>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th></th>
                    <th>Гостиница</th>
                    <th>Название</th>
                    <th>Инфо</th>
                    <th>Цена</th>
                    <th>Номер</th>
                    <th>Завтрак</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($hotels)): ?>
                    <?php foreach ($hotels as $hotel): ?>
                        <?php foreach ($hotel as $k => $item): ?>
                            <tr>
                                <td><?= ++$k ?></td>
                                <td><img src="/uploads/<?= $item->image ?>" width="100px"></td>
                                <td><?= $item->hotel->name_ru ?></td>
                                <td><?= $item->title_ru ?></td>
                                <td><?= $item->info_ru ?></td>
                                <td><?= $item->price ?></td>
                                <td><?= $item->for_person ?></td>
                                <td><?= $item->breakfast === 1 ? 'Есть' : 'Нет' ?></td>
                                <td>
                                    <button class="btn btn-primary order" data-id="<?= $item->id ?>">Забронировать номер</button>
                                    <button type="button" class="btn btn-success roomView" data-id="<?= $item->id ?>">Посмотреть комнату</button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" id="viewContent">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>