<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 05.04.2019
 * Time: 14:08
 */
?>

<?php if(!empty($users)):?>
<?php foreach ($users as $item): ?>
    <li data-id="<?= $item->id ?>"><?= $item->fullname ?></li>
<?php endforeach; ?>
<?php else: ?>
    <li><?= Yii::t('app','Ничего не найдено') ?></li>
<?php endif;?>

