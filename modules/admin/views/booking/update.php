<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 10.04.2019
 * Time: 10:45
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Изменить брон', [
    'nameAttribute' => '' . $model->room->title_ru
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Booking'), 'url' => ['booked-rooms']];
$this->params['breadcrumbs'][] = ['label' => $model->room->title_ru, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="books-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col">
                <?= $form->field($model, 'from_date')->widget(\yii\jui\DatePicker::className(),[
                    'language' => 'ru',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Выберите день заезда',
                        'required' => 'required'
                    ],
                    'clientOptions' => [
                        'minDate' => '+0',
                    ],
                    'dateFormat' => 'dd-MM-yyyy',
                ]) ?>
            </div>
            <div class="col">

                <?= $form->field($model, 'end_date')->widget(\yii\jui\DatePicker::className(),[
                        'language' => 'ru',
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'Выберите день заезда',
                            'required' => 'required'
                        ],
                        'clientOptions' => [
                            'minDate' => '+1',
                        ],
                        'dateFormat' => 'dd-MM-yyyy',
                    ]
                ) ?>
            </div>
        </div>


        <?= $form->field($model,'adult')->dropDownList([
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
            '8' => '8',
            '9' => '9',
            '10' => '10'
        ]) ?>
        <?= $form->field($model,'children')->dropDownList([
            '0' => 'Без детей',
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
            '8' => '8',
            '9' => '9',
            '10' => '10'
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>