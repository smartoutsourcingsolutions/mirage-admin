<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 04.04.2019
 * Time: 15:01
 */

use yii\jui\DatePicker;

?>

<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?= Yii::t('app','Забронировать') ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                   aria-selected="true"><?= Yii::t('app','Новый посетитель') ?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                   aria-controls="profile" aria-selected="false"><?= Yii::t('app','Существующий посетитель') ?></a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab"
                 style="margin-top: 15px">
                <h4 style="text-align: center"><?= Yii::t('app','Создать новый пользователь') ?></h4>
                <?php echo DatePicker::widget([
                    'name' => 'hidden',
                    'options' => [
                        'class' => 'hidden',
                        'placeholder' => 'Выберите день выезда',
                        'required' => 'required'
                    ],
                ]); ?>
                <?php echo DatePicker::widget([
                    'name' => 'hidden',
                    'options' => [
                        'class' => 'hidden',
                        'placeholder' => 'Выберите день выезда',
                        'required' => 'required'
                    ],
                ]); ?>

                <form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'ФИО') ?></label>
                                <input name="fullname" class="form-control" required="required"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'Гражданство') ?></label>
                                <input name="citizenship" class="form-control" required="required"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'Логин') ?></label>
                                <input name="username" class="form-control" value="<?=$username ?>" required="required"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'Пароль') ?></label>
                                <input name="password" class="form-control" value="<?=$username ?>" required="required"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'Телефон') ?></label>
                                <input name="phone" class="form-control" required="required"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'Email') ?></label>
                                <input name="email" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'Дата выезда') ?></label>
                                <?php echo DatePicker::widget([
                                    'name' => 'from_date',
                                    'value' => $from,
                                    'language' => 'ru',
                                    'options' => [
                                        'class' => 'form-control',
                                        'placeholder' => 'Выберите день заезда',
                                        'required' => 'required'
                                    ],
                                    'clientOptions' => [
                                        'minDate' => '+1',
                                    ],
                                    'dateFormat' => 'dd-MM-yyyy',
                                ]); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'Дата выезда') ?></label>
                                <?php echo DatePicker::widget([
                                    'name' => 'end_date',
                                    'language' => 'ru',
                                    'value' => $end,
                                    'options' => [
                                        'class' => 'form-control',
                                        'placeholder' => 'Выберите день выезда',
                                        'required' => 'required'
                                    ],
                                    'clientOptions' => [
                                        'minDate' => '+1',
                                    ],
                                    'dateFormat' => 'dd-MM-yyyy',
                                ]); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'Взрослых') ?></label>
                                <select name="adult" class="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'Детей') ?></label>
                                <select name="children" class="form-control">
                                    <option value="0"><?= Yii::t('app', 'Нет') ?></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= Yii::t('app','Закрыть') ?></button>
                        <button type="submit" class="btn btn-primary"><?= Yii::t('app','Бронировать') ?></button>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab" style="margin-top: 15px">
                <h4 style="text-align: center"><?= Yii::t('app','Бронировать из существующих посетителей') ?></h4>
                <form>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'ФИО') ?></label>
                                <input name="fullname" type="hidden" class="fioname">
                                <input name="searchfio" class="form-control fioSearch" placeholder="<?= Yii::t('app','Поиск по ФИО') ?>" required="required"/>
                                <div class="search_list">
                                    <ul>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'Дата выезда') ?></label>
                                <?php echo DatePicker::widget([
                                    'name' => 'from_date',
                                    'value' => $from,
                                    'language' => 'ru',
                                    'options' => [
                                        'class' => 'form-control',
                                        'placeholder' => 'Выберите день заезда',
                                        'required' => 'required'
                                    ],
                                    'clientOptions' => [
                                        'minDate' => '+1',
                                    ],
                                    'dateFormat' => 'dd-MM-yyyy',
                                ]); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'Дата выезда') ?></label>
                                <?php echo DatePicker::widget([
                                    'name' => 'end_date',
                                    'language' => 'ru',
                                    'value' => $end,
                                    'options' => [
                                        'class' => 'form-control',
                                        'placeholder' => 'Выберите день выезда',
                                        'required' => 'required'
                                    ],
                                    'clientOptions' => [
                                        'minDate' => '+1',
                                    ],
                                    'dateFormat' => 'dd-MM-yyyy',
                                ]); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'Взрослых') ?></label>
                                <select name="adult" class="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?= Yii::t('app', 'Детей') ?></label>
                                <select name="children" class="form-control">
                                    <option value="0"><?= Yii::t('app', 'Нет') ?></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= Yii::t('app','Закрыть') ?></button>
                        <button type="submit" class="btn btn-primary"><?= Yii::t('app','Бронировать') ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>