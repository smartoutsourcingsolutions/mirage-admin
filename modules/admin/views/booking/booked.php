<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 05.04.2019
 * Time: 16:48
 */

use app\models\Booking;
use yii\helpers\Html;

?>
<p>
    <?= Html::a(Yii::t('app', 'Изменить'), ['update', 'id' => $book->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $book->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]) ?>
</p>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-title" style="text-align: center; margin-bottom: 10px">
                <h4>НОМЕР ЗАБРАНИРОВАН</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <div class="row">
                        <?php if ($book): ?>
                            <div class="col">
                                <h5 style="text-align: center"><?= Yii::t('app','Информация о бронировании номера') ?></h5>
                                <table class="table">
                                    <tr>
                                        <td><?= Yii::t('app', 'Бронировал') ?></td>
                                        <td><?= !empty($book->user->fullname) ? $book->user->fullname : '' ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= Yii::t('app', 'Телефон') ?></td>
                                        <td><?= !empty($book->user->phone) ? $book->user->phone : '' ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= Yii::t('app', 'Дата заезда') ?></td>
                                        <td><?= date('d-m.Y', $book->from_date) ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= Yii::t('app', 'Дата выезда') ?></td>
                                        <td><?= date('d-m.Y', $book->end_date) ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= Yii::t('app', 'Количество дней') ?></td>
                                        <td><?= $book->days ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= Yii::t('app', 'Номер') ?></td>
                                        <td><?= !empty($book->room->for_person) ? $book->room->for_person : '' ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= Yii::t('app', 'Взрослые') ?></td>
                                        <td><?= $book->adult ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= Yii::t('app', 'Дети') ?></td>
                                        <td><?= $book->children ?></td>
                                    </tr>

                                    <tr>
                                        <td><?= Yii::t('app', 'Статус') ?></td>
                                        <td><?= $book->status == Booking::BOOKED
                                                ? '<label class="label label-info">' . Yii::t('app', 'Забронирован') . '</label>'
                                                : '<label class="label label-warning">' . Yii::t('app', 'Зарезервированный') . '</label>' ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <img src="/uploads/<?= !empty($book->room->image) ? $book->room->image : '' ?>"
                                                 width="400px">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col">
                                <h5 style="text-align: center"><?= Yii::t('app','Информация о номере в гостиницы') ?></h5>
                                <table class="table">
                                    <tr>
                                        <td><?= Yii::t('app', 'Гостиница') ?></td>
                                        <td><?= !empty($book->room->hotel->name_ru) ? $book->room->hotel->name_ru : '' ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= Yii::t('app', 'Навзвание') ?></td>
                                        <td><?= !empty($book->room->title_ru) ? $book->room->title_ru : '' ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= Yii::t('app', 'Информация') ?></td>
                                        <td><?= strip_tags($book->room->info_ru) ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= Yii::t('app', 'Цена') ?></td>
                                        <td><?= !empty($book->room->price) ? $book->room->price : '' ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= Yii::t('app', 'Завтрак') ?></td>
                                        <td><?= $book->room->breakfast == 1 ? Yii::t('app', 'Включен') : Yii::t('app', 'Не включен') ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= Yii::t('app', 'Доп инфо') ?></td>
                                        <td><?php if ($charact):
                                                foreach ($charact as $item) {
                                                    echo '- ' . $item->name_ru . '<br>';
                                                }
                                            endif; ?></td>
                                    </tr>

                                </table>

                            </div>

                        <?php endif; ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<?php if (Yii::$app->session->hasFlash('booked')): ?>
    <script>
        swal({
            title: "Успешно",
            text: "Номер успешно забронирован",
            icon: "success",
        });
    </script>
<?php endif; ?>
