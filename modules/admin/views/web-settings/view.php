<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WebSettings */

$this->title = $model->address_en;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Web Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-settings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'phone',
            'address_ru',
            'address_en',
            'email:email',
            'facebook_link',
            'instagram_link',
            'tg_link',
        ],
    ]) ?>

</div>
