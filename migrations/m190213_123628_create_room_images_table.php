<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%room_images}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%rooms}}`
 */
class m190213_123628_create_room_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%room_images}}', [
            'id' => $this->primaryKey(),
            'room_id' => $this->integer()->notNull(),
            'image' => $this->string(),
        ]);

        // creates index for column `room_id`
        $this->createIndex(
            '{{%idx-room_images-room_id}}',
            '{{%room_images}}',
            'room_id'
        );

        // add foreign key for table `{{%rooms}}`
        $this->addForeignKey(
            '{{%fk-room_images-room_id}}',
            '{{%room_images}}',
            'room_id',
            '{{%rooms}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%rooms}}`
        $this->dropForeignKey(
            '{{%fk-room_images-room_id}}',
            '{{%room_images}}'
        );

        // drops index for column `room_id`
        $this->dropIndex(
            '{{%idx-room_images-room_id}}',
            '{{%room_images}}'
        );

        $this->dropTable('{{%room_images}}');
    }
}
