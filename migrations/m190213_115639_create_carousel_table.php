<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%carousel}}`.
 */
class m190213_115639_create_carousel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%carousel}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string(),
            'title_ru' => $this->string()->notNull(),
            'title_en' => $this->string()->notNull(),
            'description_ru' => $this->string(),
            'description_en' => $this->string(),
            'order' => $this->smallInteger(),
            'status' => $this->smallInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%carousel}}');
    }
}
