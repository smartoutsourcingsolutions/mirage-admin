<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%characterics}}`.
 */
class m190213_121555_create_characterics_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%characterics}}', [
            'id' => $this->primaryKey(),
            'svg' => $this->text(),
            'name_ru' => $this->string(),
            'name_en' => $this->string(),
            'status' => $this->smallInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%characterics}}');
    }
}
