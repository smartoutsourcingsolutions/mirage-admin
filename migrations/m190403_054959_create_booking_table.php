<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%booking}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%rooms}}`
 * - `{{%user}}`
 */
class m190403_054959_create_booking_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%booking}}', [
            'id' => $this->primaryKey(),
            'room_id' => $this->integer()->notNull(),
            'user_id' => $this->integer(),
            'floor' => $this->integer(),
            'from_date' => $this->integer(),
            'end_date' => $this->integer(),
            'days' => $this->integer(),
            'adult' => $this->integer(),
            'children' => $this->integer(),
            'status' => $this->smallInteger(),
        ]);

        // creates index for column `room_id`
        $this->createIndex(
            '{{%idx-booking-room_id}}',
            '{{%booking}}',
            'room_id'
        );

        // add foreign key for table `{{%rooms}}`
        $this->addForeignKey(
            '{{%fk-booking-room_id}}',
            '{{%booking}}',
            'room_id',
            '{{%rooms}}',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-booking-user_id}}',
            '{{%booking}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-booking-user_id}}',
            '{{%booking}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%rooms}}`
        $this->dropForeignKey(
            '{{%fk-booking-room_id}}',
            '{{%booking}}'
        );

        // drops index for column `room_id`
        $this->dropIndex(
            '{{%idx-booking-room_id}}',
            '{{%booking}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-booking-user_id}}',
            '{{%booking}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-booking-user_id}}',
            '{{%booking}}'
        );

        $this->dropTable('{{%booking}}');
    }
}
