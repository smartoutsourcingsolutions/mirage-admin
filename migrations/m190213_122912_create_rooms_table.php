<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rooms}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%hotels}}`
 */
class m190213_122912_create_rooms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rooms}}', [
            'id' => $this->primaryKey(),
            'hotel_id' => $this->integer()->notNull(),
            'image' => $this->string(),
            'title_ru' => $this->string(),
            'title_en' => $this->string(),
            'info_ru' => $this->text(),
            'info_en' => $this->text(),
            'content_ru' => $this->text(),
            'content_en' => $this->text(),
            'price' => $this->integer()->notNull(),
            'for_person' => $this->integer()->notNull(),
            'breakfast' => $this->sMallInteger(),
            'characteristics' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `hotel_id`
        $this->createIndex(
            '{{%idx-rooms-hotel_id}}',
            '{{%rooms}}',
            'hotel_id'
        );

        // add foreign key for table `{{%hotels}}`
        $this->addForeignKey(
            '{{%fk-rooms-hotel_id}}',
            '{{%rooms}}',
            'hotel_id',
            '{{%hotels}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%hotels}}`
        $this->dropForeignKey(
            '{{%fk-rooms-hotel_id}}',
            '{{%rooms}}'
        );

        // drops index for column `hotel_id`
        $this->dropIndex(
            '{{%idx-rooms-hotel_id}}',
            '{{%rooms}}'
        );

        $this->dropTable('{{%rooms}}');
    }
}
