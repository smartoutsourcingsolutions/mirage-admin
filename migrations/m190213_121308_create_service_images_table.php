<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%service_images}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%service}}`
 */
class m190213_121308_create_service_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%service_images}}', [
            'id' => $this->primaryKey(),
            'service_id' => $this->integer(),
            'img' => $this->string(),
        ]);

        // creates index for column `service_id`
        $this->createIndex(
            '{{%idx-service_images-service_id}}',
            '{{%service_images}}',
            'service_id'
        );

        // add foreign key for table `{{%service}}`
        $this->addForeignKey(
            '{{%fk-service_images-service_id}}',
            '{{%service_images}}',
            'service_id',
            '{{%service}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%service}}`
        $this->dropForeignKey(
            '{{%fk-service_images-service_id}}',
            '{{%service_images}}'
        );

        // drops index for column `service_id`
        $this->dropIndex(
            '{{%idx-service_images-service_id}}',
            '{{%service_images}}'
        );

        $this->dropTable('{{%service_images}}');
    }
}
