<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%web_settings}}`.
 */
class m190213_121445_create_web_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%web_settings}}', [
            'id' => $this->primaryKey(),
            'phone' => $this->string(),
            'address_ru' => $this->string(),
            'address_en' => $this->string(),
            'email' => $this->string(),
            'facebook_link' => $this->string(),
            'instagram_link' => $this->string(),
            'tg_link' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%web_settings}}');
    }
}
