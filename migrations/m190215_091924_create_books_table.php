<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%books}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%rooms}}`
 */
class m190215_091924_create_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%books}}', [
            'id' => $this->primaryKey(),
            'room_id' => $this->integer(),
            'date' => $this->string(),
            'name' => $this->string(),
            'phone' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `room_id`
        $this->createIndex(
            '{{%idx-books-room_id}}',
            '{{%books}}',
            'room_id'
        );

        // add foreign key for table `{{%rooms}}`
        $this->addForeignKey(
            '{{%fk-books-room_id}}',
            '{{%books}}',
            'room_id',
            '{{%rooms}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%rooms}}`
        $this->dropForeignKey(
            '{{%fk-books-room_id}}',
            '{{%books}}'
        );

        // drops index for column `room_id`
        $this->dropIndex(
            '{{%idx-books-room_id}}',
            '{{%books}}'
        );

        $this->dropTable('{{%books}}');
    }
}
