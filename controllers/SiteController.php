<?php

namespace app\controllers;

use app\models\api\Allhotels;
use app\models\api\Hotel;
use app\models\api\OneService;
use app\models\api\Room;
use app\models\BaseModel;
use app\models\Blogs;
use app\models\Booking;
use app\models\Books;
use app\models\Carousel;
use app\models\forms\Signup;
use app\models\Service;
use app\models\User;
use app\models\WebSettings;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\Cors;
use yii\rest\ActiveController;

class SiteController extends ActiveController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter'] = [
            'class' => Cors::class,
            'cors' => [
                'Origin' => ['http://mirage-front.sos.uz', 'http://localhost:8080'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST'],
            ]
        ];
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => ['change-info','get-user'],  //access controller
        ];
//        $behaviors['authenticator']['except'] = ['options'];

        return $behaviors;
    }

    public $modelClass = 'app\models\Hotels';


    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    /**
     * @return string
     */
    public function actionIndex()
    {
        return 'OK';
    }

    /** Carousel
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionCarousel()
    {
        $carousel = Carousel::find()
            ->where(['status' => BaseModel::STATUS_ACTIVE])
            ->orderBy(['order' => SORT_ASC])
            ->all();
        return $carousel;
    }


    public function actionAbout()
    {
        $res = Carousel::findOne(1);
        return $res;
    }

    public function actionNews()
    {
        $res = Carousel::find()
            ->where(['status' => 1])
            ->orderBy(['order' => SORT_DESC])
            ->all();
        return $res;
    }

    /**
     * All Hotels with rooms and characteristics
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionHotels()
    {
        $hotels = Hotel::find()->where(['status' => BaseModel::STATUS_ACTIVE])->all();
        return $hotels;
    }

    /** All hotels with characteristics
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionAllHotels()
    {
        $hotels = Allhotels::find()->where(['status' => BaseModel::STATUS_ACTIVE])->all();
        return $hotels;

    }

    /**
     * One Hotel with rooms and characteristics
     * @param $id
     * @return array|null|string
     */
    public function actionOneHotel($id)
    {
        if ($id) {
            $hotel = Hotel::find()
                ->where(['id' => $id])
                ->andWhere(['status' => BaseModel::STATUS_ACTIVE])
                ->one();
            return $hotel;
        }
        return [];
    }

    /**
     * Get All rooms
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionRooms()
    {
        $rooms = Room::find()->all();
        return $rooms;
    }

    /** One Room
     * @param $id
     * @return null|object
     */
    public function actionOneRoom($id)
    {
        $room = Room::findOne($id);
        return $room;
    }


    /** All services
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionServices()
    {
        $services = Service::find()->where(['status' => BaseModel::STATUS_ACTIVE])->all() ?: [];
        return $services;
    }

    /** One Service
     * @param $id
     * @return array|object
     */
    public function actionOneService($id)
    {
        $service = OneService::findOne($id);
        if ($service) {
            return $service;
        }
        return [];
    }


    /** 3 Blogs for Home page
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionHomeBlog()
    {
        $blog = Blogs::find()
            ->where(['status' => BaseModel::STATUS_ACTIVE])
            ->orderBy(['id' => SORT_DESC])
            ->limit(3)
            ->all();
        return $blog;
    }

    /** All Blogs
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionBlogs()
    {
        $blog = Blogs::find()
            ->where(['status' => BaseModel::STATUS_ACTIVE])
            ->orderBy(['id' => SORT_DESC])
            ->all();
        return $blog;
    }

    /**
     * One Blog
     * @param $id
     * @return array|null|static|object
     */
    public function actionOneBlog($id)
    {
        if ($id) {
            $blog = Blogs::findOne($id);
            return $blog;
        }
        return [];
    }

    /** WebSite Setting info
     * @return null|static|object
     */
    public function actionWebSetting()
    {
        $settings = WebSettings::findOne(1);
        return $settings;
    }

    public function actionBooks()
    {
        $book = new Books();
        $book->name = Yii::$app->request->post('name');
        $book->phone = Yii::$app->request->post('phone');
        $book->date = Yii::$app->request->post('date');
        $book->room_id = Yii::$app->request->post('roomID');
        if ($book->save(false)) {
            Yii::$app->mailer->compose()
                ->setFrom('nazirovlix@mail.ru')
                ->setTo('nazirovlix@mail.ru')
                ->setSubject('Бронирование на сайте www.mirage.uz')
                ->setHtmlBody('Брон №: <b>' . $book->id . '</b><br>На дату: ' . $book->date . '<br>Комната: ' . $book->room->title_ru . '<br>Имя: ' . $book->name . '<br>Телефон: ' . $book->phone)
                ->send();
            return true;
        }
        return false;
    }

    // ------------------- Booking --------------------- //

    /** Search Free rooms
     * @return array
     *
     *   [hotel_1_id] = [
     *                   [0] => [ room1 Info]
     *                   [1] => [ room2 Info]
     *                 ],
     *   [hotel_2_id] = [
     *                   [0] => [ room3 Info]
     *                   [1] => [ room4 Info]
     *                 ]
     *
     */
    public function actionSearch()
    {
        $get = Yii::$app->request->get();
        $booking = new Booking();
        $hotels = $booking->getRooms(strtotime($get['from_date']), strtotime($get['end_date']), $get['hotel_id'], $get['number']);
        return $hotels ?: [];
    }


    /** Booking if user SigIn
     *  GET request
     *  $get['token']
     *  POST request
     *  $post['room_id']
     *  $post['floor']
     *  $post['from_date']
     *  $post['end_date']
     *  $post['adult']
     *  $post['children']
     *
     * @return bool
     */
    public function actionBooking($token)
    {
        $user = User::find()
            ->where(['auth_key' => (string)$token])
            ->one();
        $this->enableCsrfValidation = false;
        $booking = new Booking();
        if ($booking->booking(Yii::$app->request->post(),$user->id))
            return true;
        return false;
    }

    /** Booking if User don't SignIn
     *  GET request for USER
     *  $get['username']
     *  $get['email']
     *  $get['fullname']
     *  $get['citizenship']
     *  $get['phone']
     *  $get['password']
     *  GET request for BOOKING
     *  $get['room_id']
     *  $get['user_id']
     *  $get['floor']
     *  $get['from_date']
     *  $get['end_date']
     *  $get['adult']
     *  $get['children']
     * @return bool
     */
    public function actionSignUpAndBooking()
    {
        $this->enableCsrfValidation = false;
        $user = new Signup();
        $booking = new Booking();
        $userId = $user->saveNew(Yii::$app->request->post());
        if ($userId && $booking->booking(Yii::$app->request->post(), $userId))
            return true;
        return false;
    }

    // ------------------- USER --------------------- //

    /** Sign Up new user
     *  GET request
     *  $get['username']
     *  $get['email']
     *  $get['fullname']
     *  $get['citizenship']
     *  $get['phone']
     *  $get['password']
     * @return bool|object
     */
    public function actionSignUp()
    {
        $this->enableCsrfValidation = false;
        $user = new Signup();
        $userId = $user->saveNew(Yii::$app->request->post());
        if ($userId) {
            $user = User::findOne($userId);
            if ($user)
                return $user->auth_key;
        }
        return false;
    }

    /**
     *  Get USER info with Booking History
     * @param $id
     * @return bool|object
     */
    public function actionGetUser($token)
    {
        $user = User::find()
            ->where(['auth_key' => (string)$token])
            ->asArray()
            ->one();
        if ($user) {
            $booking = Booking::find()
                ->where(['user_id' => $user['id']])
                ->all();
            $user['history'] = $booking;
            unset($user['password_hash']);
            unset($user['password_reset_token']);
            unset($user['id']);
            return $user;
        }
        return false;
    }


    /**
     *  Change user info
     *  GET request
     *  $get['username']
     *  $get['email']
     *  $get['fullname']
     *  $get['citizenship']
     *  $get['phone']
     *  $get['password']
     *
     * @param $id
     * @return bool|object
     */
    public function actionChangeInfo($token)
    {
        $this->enableCsrfValidation = false;
        $user = User::findOne(['auth_key' => $token]);
        if ($user) {
            $changedUser = $user->changeInfo($user->id, Yii::$app->request->post());
            $user = User::find()
                ->where(['auth_key' => $changedUser->auth_key])
                ->asArray()
                ->one();
            unset($user['password_hash']);
            unset($user['password_reset_token']);
            unset($user['id']);
            return $user;
        }
        return false;
    }

    /** Login User
     * $get['username']
     * $get['password']
     *
     * @return bool|string
     */
    public function actionLogin()
    {
        $username = Yii::$app->request->get('username');
        $password = Yii::$app->request->get('password');
        $user = User::findOne(['username' => $username]);
        if($user && $user->validatePassword($password)){
            return $user->auth_key;
        }
        return false;

    }


}
