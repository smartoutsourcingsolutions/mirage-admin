<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 25.03.2019
 * Time: 15:18
 */
namespace app\controllers;

use yii\filters\auth\HttpBearerAuth;
use yii\web\Controller;

class FrontController extends Controller
{

    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionError()
    {
        echo 'error';
    }
}